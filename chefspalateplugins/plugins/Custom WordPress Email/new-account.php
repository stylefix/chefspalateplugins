<?php
/**
 * Created by PhpStorm.
 * User: aneeshsingh
 * Date: 13/5/18
 * Time: 1:48 PM
 */

/*
Plugin Name: [StyleFix] Custom New User Email
Description: Changes the copy in the email sent out to new users
*/

// Redefine user notification function
if ( !function_exists( 'wp_new_user_notification' ) ) {
    function wp_new_user_notification( $studentID, $plaintext_pass = '' ) {
        $student            = new WP_User($studentID);
        $student_data       = get_userdata( $studentID );
        $firstname          = $student_data->first_name;
        $student_login      = stripslashes( $student_data->user_login );

        // URLs
        $site_url = site_url();
        $ads_url = site_url( 'ads/' );
        $login_url = site_url();

        // Email variables
        $headers            = 'From: EXAMPLE.INFO <info@example.info>' . "rn";
        $blog_name          = get_option( 'blogname' );
        $admin_subject      = 'New User Registration on ' . $blog_name;
        $welcome_subject    = 'An account has been created for you!';
        $welcome_email      = stripslashes( $student_data->user_email );
        $admin_email        = get_option('admin_email');

        $admin_message =
            <<<EOT
                <!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml"><head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                </head><body>
                <div class="content">
                    <div class="wrapper">
                        <p>New user registration on your blog: {$blog_name}.</p>
                        <p>Username: {$student_login}</p>
                        <p>Email: {$welcome_email}</p>
                    </div>
                </div>
                </body></html>
EOT;

        $welcome_message =
            <<<EOT
                   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>A Simple Responsive HTML Email</title>
    <style type="text/css">
        body {margin: 0; padding: 0; min-width: 100%!important;}
        img {height: auto;}
        .content {width: 100%; max-width: 600px;}
        .header {padding: 40px 50px 20px 50px;}
        .innerpadding {
            padding: 0px 50px 30px 50px;
        }
        .button a {color: #ffffff; text-decoration: none;}
        .footer {padding: 20px 50px 15px 50px;}
        .footercopy {font-family: sans-serif; font-size: 14px; color: #ffffff;}
        .footercopy a {color: #ffffff; text-decoration: underline;}

        @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
            body[yahoo] .button a {background-color: #e05443; padding: 15px 15px 13px!important;}
        }
    </style>
</head>

<body yahoo bgcolor="#f4f4f4">
<table width="100%" bgcolor="#f4f4f4" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <!--[if (gte mso 9)|(IE)]>
            <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
            <![endif]-->
            <table bgcolor="#f4f4f4" class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>

            <table bgcolor="#ffffff" class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td bgcolor="#ffffff" class="header">

                        <table width="70" align="center" border="0" cellpadding="0" cellspacing="0" style="padding-bottom : 25px; ">
                            <tr>
                                <td>

                                    <img src="https://stylefix.co/wp-content/uploads/2018/05/Stylefix-logo-top.jpg" class="fix" height="19" width="172" alt="Style Fix Logo"/>
                                </td>
                            </tr>
                        </table>

                        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="background-color: black;" height="4px">

                                </td>
                            </tr>
                        </table>


                        <table width="70" align="center" border="0" cellpadding="0" cellspacing="0" style="padding-top : 25px; ">
                            <tr>
                                <td>
                                  <img src="https://stylefix.co/wp-content/uploads/2018/05/Your-Personal-Stylis.jpg" height="14" width="258" class="fix" alt="YOUR PERSONAL STYLIST"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <!--Header-->
                <tr>
                    <td align="center" valign="top">
                        <table align="center" width="600" border="0" cellspacing="0" cellpadding="0" style="width:600px;">
                            <tr>
                                <td align="center" valign="top" background="https://stylefix.co/wp-content/uploads/2018/05/Group.jpg" height="900" style="background-repeat:no-repeat; background-position:center top; background-color:#ffffff;"><!--[if gte mso 9]>
                                    <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:550px;height:825px;">
                                        <v:fill type="tile" src="https://stylefix.co/wp-content/uploads/2018/05/Group.jpg" color="#ffffff" />
                                        <v:textbox inset="0,0,0,0">
                                    <![endif]-->

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr>
                                            <td width="90" style="width:90px;" >&nbsp;</td>
                                            <td align="center" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                <tr>
                                                    <td height="105" style="height:105px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="105" style="height:105px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="10"  style="font-size:1px; line-height:1px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="75"  style="font-size:1px; line-height:1px;">&nbsp;</td>

                                                    <td align="center" valign="top"  style="font-family: Palatino, Georgia; font-size:18px; line-height:22px; ">
                                                        <p>  &nbsp;</p>
                                                        <p>An account was created for you at <a href="https://stylefix.co/">https://stylefix.co/</a> </p>
                                                        <p>Your details are:</p>
                                                        <p>Username - '.$student_login.'</p>
                                                        <p>Email - '.$welcome_email.'</p>
                                                        <p>Password - '.$plaintext_pass.'</p>
                                                        <p>If this wasn\'t you please let us know at hello@stylefix.co</p>

                                                    </td>
                                                    <td width="75"  style="font-size:1px; line-height:1px;">&nbsp;</td>

                                                </tr>
                                                <tr>
                                                    <td height="25" style="height:25px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="10"  style="font-size:1px; line-height:1px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="10"  style="font-size:1px; line-height:1px;">&nbsp;</td>
                                                </tr>
                                            </table></td>
                                            <td width="90" style="width:90px;" >&nbsp;</td>
                                        </tr>
                                    </table>

                                    <!--[if gte mso 9]>
                                    </v:textbox>
                                    </v:rect>
                                    <![endif]--></td>
                            </tr>
                        </table></td>
                </tr>
                <!--//Header-->



                <tr>
                    <td class="footer" bgcolor="#ffffff" border="0">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" class="footercopy">
                                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="background-color: black;" height="4px">

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="padding: 20px 0 0 0;">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="500" style="text-align: center; padding: 0 10px 0 10px;font-family: Palatino, Georgia; line-height: 20px;">

                                                <p>This email was sent to '.$welcome_email.'.
                                                </p>
                                                <p>To ensure that our messages get to you (and don\'t go to your junk or bulk email folders),
                                                    please add hello@stylefix.co to your address book.</p>

                                                <p>To unsubscribe click here</p>
                                                <p>To view our Privacy Policy click here</p>

                                                <p>STYLEFIX.CO</p>
                                            </td>

                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table bgcolor="#f4f4f4" class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>

            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
</table>
</body>
</html>;
EOT;
        wp_mail( $admin_email, $admin_subject, $admin_message, $headers );
        wp_mail( $welcome_email, $welcome_subject, $welcome_message, $headers );
    } // End of wp_new_user_notification()
}

?>