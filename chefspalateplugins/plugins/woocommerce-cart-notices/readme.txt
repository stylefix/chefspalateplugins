=== WooCommerce Cart Notices ===
Author: skyverge
Tags: woocommerce
Requires at least: 4.4
Tested up to: 4.9.6

Add dynamic notices above the cart and checkout to help increase your sales!

See http://docs.woothemes.com/document/woocommerce-cart-notices/ for full documentation.

== Installation ==

1. Upload the entire 'woocommerce-cart-notices' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
